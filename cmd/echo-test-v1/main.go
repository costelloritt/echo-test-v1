package main

import (
	"fmt"
	"log"
	"os"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"go.uber.org/zap"

	"../../internal/echo-test-v1/config"
	"../../internal/echo-test-v1/routes"
)

// APIConfig is the struct representing API configuration information.
type APIConfig struct {
	Port string `json:"port"`
}

var sl *zap.SugaredLogger

func init() {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("cant initialize zap logger: %s", err.Error())
	}
	defer logger.Sync()
	sl = logger.Sugar()
	sl.Infow("Sugar Logger Loaded",
		"loadStatus", true)
}

var apiConfig config.APIConfig

func init() {
	afPath := os.Args[1]
	err := config.LoadAPIConfig(afPath, &apiConfig)
	if err != nil {
		sl.Fatalf("failed to load api config: %s", err.Error())
	}
	sl.Infow("api config loaded",
		"port", apiConfig.Port)
}

func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	routes.LoadRoutes(e)
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%s", apiConfig.Port)))
}
