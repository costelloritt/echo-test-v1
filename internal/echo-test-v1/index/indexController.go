package index

import (
	"net/http"

	"github.com/labstack/echo"
)

type IndexController struct{}

// Status returns the status of the application.
func (i *IndexController) Status(c echo.Context) error {
	return c.String(http.StatusOK, "OK - OK")
}
