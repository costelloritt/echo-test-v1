package config

import (
	"encoding/json"
	"io/ioutil"
)

// APIConfig is the struct representing API configuration information.
type APIConfig struct {
	Port string `json:"port"`
}

// LoadAPIConfig loads API configuration information from a specified file
// into the APIConfig struct.
func LoadAPIConfig(filePath string, af *APIConfig) error {
	dat, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}

	err = json.Unmarshal(dat, &af)
	if err != nil {
		return err
	}
	return nil
}
