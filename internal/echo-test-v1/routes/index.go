package routes

import (
	"github.com/labstack/echo"

	"../index"
)

// LoadRoutes loads all of the application endpoints.
func LoadRoutes(e *echo.Echo) {
	i := index.IndexController{}

	e.GET("/status", i.Status)
}
